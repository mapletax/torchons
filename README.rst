#################################
T4127 Payroll Deductions Formulas
#################################

This repository contains a collection of T4127 revisions.

The last ones can be obtained from
https://www.canada.ca/en/revenue-agency/services/tax/businesses/topics/payroll/t4127-payroll-deductions-formulas-computer-programs.html

Each file in this repo comes with a metalink file providing its origin and checksum at time of download.


Notes
#####

- While the most recent versions can be downloaded from canada.ca,
  old ones sadly can't.

  Some internet archeology was used to retrieve older versions, mostly from archive.org.

- The HTML version of the formulas was sometimes versioned but mostly not.

  So it should be archived from snapshots, and the snapshots should ideally
  done by a reliable third party,
  eg. archive.org:

  https://web.archive.org/web/20240416050917/https://www.canada.ca/en/revenue-agency/services/forms-publications/payroll/t4127-payroll-deductions-formulas/t4127-jan/t4127-jan-payroll-deductions-formulas-computer-programs.html

